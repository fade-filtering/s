window.fadeFiltering = function(owo){
	let btns = owo.buttons;
	let items = owo.items;
	let attr = owo.attr;
	
	// make sure buttons and items exist
	btns = document.querySelectorAll(btns);
	items = document.querySelectorAll(items);
	
	// items parent
	let itemsParent;
	items ? itemsParent = items[0].parentNode : "";
	
	// get gallery fade speed
	let gf = getComputedStyle(document.documentElement).getPropertyValue("--Gallery-Fade-Speed");
	let gf_num = Number(gf.replace(/[^\d\.]*/g,""));
	let gf_sfx = gf.replace(/[^A-Za-z]+/g,"");
	gf_sfx == "s" ? gf = gf_num * 1000 : gf = gf_num;
	
	btns ? btns.forEach(btn => {
		// get current <button>'s filter name
		let btnTag = btn.matches(`[${attr}]`);
		btnTag && btn.getAttribute(attr).trim() !== "" ? btnTag = btn.getAttribute(attr).trim() : "";
		//console.log(btnTag);
		
		btn.addEventListener("click", () => {
			if(!btn.matches(".btn-active")){
				itemsParent.classList.add("fade-out");
				setTimeout(() => {
					itemsParent.classList.remove("fade-out")
				},gf);
				
				items ? items.forEach(item => {
					// reset all items
					item.classList.remove("present");

					// get current item's filters
					let itemTags = item.matches(`[${attr}]`);
					itemTags && item.getAttribute(attr).trim() !== "" ? itemTags = item.getAttribute(attr).trim().replaceAll(", ",",") : "";
					itemTags = itemTags.split(",");
					//console.log(itemTags);

					// scan every item's filters
					// check if any 1 filter matches the current btn
					let scanIndex = itemTags.findIndex(uwu => {
						return uwu === btnTag
					})

					if(scanIndex > -1){
						item.classList.add("present")
					}
				}) : "" // end items each

				// show all matched items,
				// hide the rest
				setTimeout(() => {
					items ? items.forEach(v => {
						if(v.matches(".present")){
							v.style.display = "block"
						} else {
							v.style.display = "none"
						}
					}) : ""
				},gf + 1)
			}//end: if not dupe-click
			
			// button click: add active indicator
            // remove .btn-active from siblings (not 'this')
            [...btn.parentNode.children].filter((b) => b !== btn).forEach(rest => {
                rest.classList.remove("btn-active")
            })
            
            btn.classList.add("btn-active");
			
		})//end <button> click
	}) : "" // end <button> each
}//end entire function
