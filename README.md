### fadeFiltering() plugin

**Description:** simple filtering plugin via any custom attribute, with fade transition  
**Author:** HT (@&#x200A;glenthemes)

#### Demo:
🪴&ensp;[fade-filtering.gitlab.io/s/demo](https://fade-filtering.gitlab.io/s/demo)

#### Requirements:
* basic–intermediate knowledge in HTML/CSS
* your filter buttons should be wrapped in a parent div
* your filter items should be wrapped in a parent div

Example markup:
```html
<div class="buttons-wrapper">
    <button>filter name #1</button>
    <button>filter name #2</button>
    <button>filter name #3</button>
</div>

<div class="items-wrapper">
    <img src="...">
    <img src="...">
    <img src="...">
</div>
```

#### Step 1:
Initialize the script:
```html
<!--✻✻✻✻✻✻  FADE FILTERING by @glenthemes  ✻✻✻✻✻✻-->
<script src="//fade-filtering.gitlab.io/s/init.js"></script>
<script>
document.addEventListener("DOMContentLoaded", () => {
	fadeFiltering({
		buttons: ".buttons-wrapper button", // selector for your filter buttons
		items: ".items-wrapper img", // selector for the items you're filtering
		attr: "tags" // custom attribute where you'll add your different tags
	})
})
</script>
```

#### Step 2:
Add this CSS (inside a `<style>`):
```css
:root {
	--Gallery-Fade-Speed:420ms; /* change the fade transition speed here */
}

.items-wrapper.fade-out {
	opacity:0;
}

.items-wrapper,
.items-wrapper.fade-out {
	transition:opacity var(--Gallery-Fade-Speed) ease-in-out;
}
```
In the above code, change `.items-wrapper` to your **buttons' wrapper selector**.  

Do not change `.fade-out`.

#### Step 3:
Assign every button with a filter tag, and use a custom attribute for this.  
In this example, I'm just using `tags`.  

If you change this to something else,
* you need to update it in the `attr: "tags"` script line from **Step 1**.
* so e.g. changing it to `cool-categories` would change the script line to `attr: "cool-categories"`

Each button should only have one filter.
```html
<div class="buttons-wrapper">
    <button tags="all">display text for this filter</button>
    <button tags="food">display text for this filter</button>
    <button tags="nature">display text for this filter</button>
    <button tags="buildings">display text for this filter</button>
    <button tags="people">display text for this filter</button>
</div>
```

#### Step 4:
Assign filters to your items. You can assign more than one by separating them with a comma `,`&ensp;:revolving_hearts:
```html
<div class="items-wrapper">
    <img src="..." tags="all, buildings, people">
    <img src="..." tags="all, nature">
    <img src="..." tags="all, people, food">
</div>
```

Notice that I've added an `all` filter to all my items, as well as an `all` button to match. You don't have to include it, but I recommend doing so because it allows your users to reset the filters. Without it, they would need to refresh the page if they want to start over.

---

#### Issues & Troubleshooting:
[discord.gg/RcMKnwz](https://discord.gg/RcMKnwz)

---

#### Found this tool helpful?
Please consider sending me a donation, thank you!  
&boxh;&ensp;[ko-fi.com/glenthemes](https://ko-fi.com/glenthemes) :coffee:
